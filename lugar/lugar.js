const axios = require('axios')

const getLugarLatLng = async (dir) => {
    const encodedUlr = encodeURI(dir)
    
    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodedUlr}`,
        headers: {'X-RapidAPI-Key': 'bfd57bad89msha166eb700914354p17735cjsn9dea6ec4b7d7'}
    })
    
    
    const resp = await instance.get()
        
    if(resp.data.Results.length === 0){
        throw new Error(`No hay resultado para ${dir}`)
    }

    const data = resp.data.Results[0]
    const direccion = data.name
    const latitud = data.lat
    const longitud = data.lon

    return {
        direccion,
        latitud,
        longitud
    }
}


module.exports = {
    getLugarLatLng
}



