const axios = require('axios')


const getClima = async (latitud, longitud) => {
    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${latitud}&lon=${longitud}&appid=2761ef3e4f4f7f518a8b3384512869f8&units=metrics`)
    return resp.data.main.temp
}


module.exports = {
    getClima
}