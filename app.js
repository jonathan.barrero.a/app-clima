const lugar = require('./lugar/lugar')
const clima = require('./clima/clima')

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Direccion de la ciudad',
        demand: true
    }
}).argv

// lugar.getLugarLatLng(argv.direccion)
//             .then( console.log)


// clima.getClima(40.750000, -74.000000)
//             .then(console.log)
//             .catch(console.log)

const getInfo = async (dir) => {

    try {
        const coords = await lugar.getLugarLatLng(dir)
        const temp = await clima.getClima(coords.latitud, coords.longitud)
        return `El clima de ${coords.direccion} es de ${temp}`
    } catch (error) {
        return `No se pudo determinar el clima de ${dir}`
    }
}

getInfo(argv.direccion)
    .then(console.log)
    .catch(console.log)
